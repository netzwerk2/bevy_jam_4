use bevy::prelude::*;

use bevy_jam_4::GamePlugin;

fn main() {
    App::new().add_plugins(GamePlugin).run();
}
