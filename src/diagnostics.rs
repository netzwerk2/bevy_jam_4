use crate::game_state::GameState;
use bevy::diagnostic::{Diagnostic, DiagnosticId, Diagnostics, RegisterDiagnostic};
use bevy::prelude::*;
use orbit_tessellation::Orbit;

use crate::kessler_syndrome::Debris;

pub struct DiagnosticsPlugin;

impl Plugin for DiagnosticsPlugin {
    fn build(&self, app: &mut App) {
        app.register_diagnostic(
            Diagnostic::new(Self::DEBRIS_COUNT, "debris_count", 1).with_smoothing_factor(0.0),
        )
        .add_systems(
            Update,
            Self::diagnostic_system.run_if(in_state(GameState::InGame)),
        );
    }
}

impl DiagnosticsPlugin {
    pub const DEBRIS_COUNT: DiagnosticId =
        DiagnosticId::from_u128(246421193218505918715524458851856463983);

    pub fn diagnostic_system(
        mut diagnostics: Diagnostics,
        debris_orbit_query: Query<&Orbit, With<Debris>>,
    ) {
        let debris_count = debris_orbit_query.iter().len();

        diagnostics.add_measurement(Self::DEBRIS_COUNT, || debris_count as f64);
    }
}
