use bevy::prelude::shape::Plane;
use bevy::prelude::*;
use bevy_mod_picking::prelude::{Listener, Pointer, Select};
use bevy_mod_picking::PickableBundle;
use bevy_polyline::prelude::*;
use orbit_tessellation::Orbit;

use crate::assets::{OrbitAssets, SpriteAssets};
use crate::game_state::GameState;
use crate::maneuver::ManeuverInput;
use crate::time::{IngameTime, TimeMultiplier};
use crate::ui::SATELLITE_Z_INDEX;
use crate::EARTH_MU;

pub struct SatellitePlugin;

impl Plugin for SatellitePlugin {
    fn build(&self, app: &mut App) {
        app.init_resource::<SelectedSatellite>().add_systems(
            Update,
            (update_satellite_positions, update_orbit_for_satellite)
                .run_if(in_state(GameState::InGame)),
        );
    }
}
#[derive(Component)]
pub struct Satellite {
    pub delta_v: f64,
}

impl Default for Satellite {
    fn default() -> Self {
        Self { delta_v: 30.0 }
    }
}

#[derive(Bundle)]
pub struct SatelliteBundle {
    pub pbr: PbrBundle,
    pub orbit: Orbit,
    pub pickable_bundle: PickableBundle,
    pub satellite: Satellite,
}

impl SatelliteBundle {
    pub fn new(orbit: Orbit, assets: &SpriteAssets, meshes: &mut Assets<Mesh>) -> SatelliteBundle {
        SatelliteBundle {
            orbit,
            pbr: PbrBundle {
                mesh: meshes.add(Mesh::from(Plane::from_size(3e6))),
                material: assets.satellite_material.clone_weak(),
                transform: Transform::from_xyz(0.0, SATELLITE_Z_INDEX, 0.0),
                ..default()
            },
            pickable_bundle: PickableBundle::default(),
            satellite: Satellite::default(),
        }
    }
}

#[derive(Debug, Default, Deref, DerefMut, Resource)]
pub struct SelectedSatellite(pub Option<Entity>);

#[derive(Component, Deref, DerefMut)]
pub struct OrbitForSatellite(pub Entity);

pub fn set_selected_satellite(
    mut time_multiplier: ResMut<TimeMultiplier>,
    event: Listener<Pointer<Select>>,
    mut selected_satellite: ResMut<SelectedSatellite>,
) {
    **selected_satellite = Some(event.listener());
    **time_multiplier = 0.0;
}

pub fn remove_selected_satellite(
    mut time_multiplier: ResMut<TimeMultiplier>,
    mut selected_satellite: ResMut<SelectedSatellite>,
    mut maneuver_input: ResMut<ManeuverInput>,
    ingame_time: Res<IngameTime>,
    mut satellites: Query<(&mut Orbit, &GlobalTransform, &mut Satellite)>,
) {
    let Some(entity) = **selected_satellite else {
        return;
    };
    let (mut orbit, global_transform, mut satellite) = satellites.get_mut(entity).unwrap();

    let mut state_vectors = orbit.calculate_state_vectors(**ingame_time);
    let delta_v = global_transform
        .compute_matrix()
        .transform_vector3(Vec3::new(
            maneuver_input.radial_in as f32,
            0.0,
            -maneuver_input.prograde as f32,
        ))
        .as_dvec3();

    if delta_v.length() <= satellite.delta_v {
        state_vectors.velocity += delta_v;
        *orbit = Orbit::from_state_vectors(state_vectors, EARTH_MU, **ingame_time);

        satellite.delta_v -= delta_v.length();
    }

    *maneuver_input = ManeuverInput::default();
    *time_multiplier = TimeMultiplier::default();
    **selected_satellite = None;
}

fn update_orbit_for_satellite(
    selected_satellite: Res<SelectedSatellite>,
    maneuver_input: ResMut<ManeuverInput>,
    ingame_time: Res<IngameTime>,
    orbit_assets: Res<OrbitAssets>,
    satellite_query: Query<(&Orbit, &GlobalTransform, &Satellite)>,
    mut orbit_query: Query<
        (
            &mut Orbit,
            &OrbitForSatellite,
            &mut Handle<PolylineMaterial>,
        ),
        Without<Satellite>,
    >,
) {
    if let Some(selected_satellite) = **selected_satellite {
        let (satellite_orbit, global_transform, satellite) =
            satellite_query.get(selected_satellite).unwrap();

        let (mut polyline_orbit, _, mut material) = orbit_query
            .iter_mut()
            .find(|(_, OrbitForSatellite(e), _)| *e == selected_satellite)
            .unwrap();

        let mut state_vectors = satellite_orbit.calculate_state_vectors(**ingame_time);
        let delta_v = global_transform
            .compute_matrix()
            .transform_vector3(Vec3::new(
                maneuver_input.radial_in as f32,
                0.0,
                -maneuver_input.prograde as f32,
            ))
            .as_dvec3();

        if delta_v.length() > satellite.delta_v {
            *material = orbit_assets.invalid_orbit.clone_weak();
        } else {
            *material = orbit_assets.valid_orbit.clone_weak();
        }

        state_vectors.velocity += delta_v;

        *polyline_orbit = Orbit::from_state_vectors(state_vectors, EARTH_MU, **ingame_time);

        return;
    }

    for (mut orbit, OrbitForSatellite(e), mut material) in orbit_query.iter_mut() {
        *orbit = *satellite_query.get(*e).unwrap().0;
        *material = orbit_assets.valid_orbit.clone_weak();
    }
}

fn update_satellite_positions(
    ingame_time: Res<IngameTime>,
    mut orbit_query: Query<(&Orbit, &mut Transform), With<Satellite>>,
) {
    for (orbit, mut transform) in orbit_query.iter_mut() {
        let state_vectors = orbit.calculate_state_vectors(**ingame_time);
        transform.translation =
            state_vectors.position.as_vec3() + Vec3::new(0.0, SATELLITE_Z_INDEX, 0.0);
        transform.rotation = Transform::default()
            .looking_to(state_vectors.velocity.as_vec3(), Vec3::Y)
            .rotation;
    }
}
