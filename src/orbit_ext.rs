use bevy::math::{DVec2, DVec3};
use orbit_tessellation::{Orbit, TessellationSettings};

use crate::orbit_intersection::Collision;

pub const TESSELATION_SETTINGS: TessellationSettings = TessellationSettings {
    initial_points: 64,
    max_area: 50.0,
    max_iterations: 4,
};

pub trait OrbitExt {
    fn semi_latus_rectum(&self) -> f64;

    fn intersect(&self, other: &Orbit) -> Option<DVec2>;

    fn position_from_angle(&self, phi: f64) -> DVec3;

    fn mean_angular_velocity(&self) -> f64;

    fn time_till_phi(&self, phi: f64, time: f64) -> f64;

    fn intersection_times(&self, other: &Orbit, time: f64) -> Option<(DVec2, DVec2)>;

    fn closest_intersection(&self, other: &Orbit, time: f64) -> Option<Intersection>;
    fn collisions(
        &self,
        other: &Orbit,
        time: f64,
        min_collision_distance: f64,
    ) -> [Option<Collision>; 4];
}

impl OrbitExt for Orbit {
    fn semi_latus_rectum(&self) -> f64 {
        self.semi_major_axis * (1.0 - self.eccentricity.powi(2))
    }

    fn intersect(&self, other: &Orbit) -> Option<DVec2> {
        let (p1, e1, a1) = (
            self.semi_latus_rectum(),
            self.eccentricity,
            self.argument_of_periapsis,
        );
        let (p2, e2, a2) = (
            other.semi_latus_rectum(),
            other.eccentricity,
            other.argument_of_periapsis,
        );

        let p1e2 = p1 * e2;
        let p2e1 = p2 * e1;

        let modulus = (p1e2.powi(2) + p2e1.powi(2) - 2.0 * p1e2 * p2e1 * (a1 - a2).cos()).sqrt();
        let angle = (p1 * e2 * a2.sin() - p2 * e1 * a1.sin())
            .atan2(p1 * e2 * a2.cos() - p2 * e1 * a1.cos());
        let cos_delta = (p2 - p1) / modulus;

        if cos_delta.abs() > 1.0 {
            None
        } else {
            let acos = cos_delta.acos();

            Some(DVec2::new(angle - acos, angle + acos))
        }
    }

    fn position_from_angle(&self, phi: f64) -> DVec3 {
        let r = self.semi_latus_rectum()
            / (1.0 + self.eccentricity * (phi - self.argument_of_periapsis).cos());

        DVec3::new(r * phi.cos(), 0.0, r * phi.sin())
    }

    fn mean_angular_velocity(&self) -> f64 {
        (self.gravitational_parameter * self.semi_major_axis.powi(-3)).sqrt()
    }

    fn time_till_phi(&self, phi: f64, time: f64) -> f64 {
        let delta_time = time - self.time;
        let phi = self.argument_of_periapsis + phi;

        let eccentric_anomaly = 2.0
            * (((1.0 - self.eccentricity) / (1.0 + self.eccentricity)).sqrt() * (phi * 0.5).tan())
                .atan();

        ((eccentric_anomaly - self.eccentricity * eccentric_anomaly.sin() - self.mean_anomaly)
            / self.mean_angular_velocity()
            - delta_time)
            .rem_euclid(self.period())
    }

    fn intersection_times(&self, other: &Orbit, time: f64) -> Option<(DVec2, DVec2)> {
        let angles = self.intersect(other)?;

        let time1 = self.time_till_phi(angles.x, time);
        let time2 = self.time_till_phi(angles.y, time);

        let time1_other = other.time_till_phi(angles.x, time);
        let time2_other = other.time_till_phi(angles.y, time);

        Some((
            DVec2::new(time1, time2) + DVec2::splat(time),
            DVec2::new(time1_other, time2_other) + DVec2::splat(time),
        ))
    }

    fn closest_intersection(&self, other: &Orbit, time: f64) -> Option<Intersection> {
        let intersection_times = self.intersection_times(other, time)?;

        let (time1, time2) = (intersection_times.0.x, intersection_times.0.y);
        let (time1_other, time2_other) = (intersection_times.1.x, intersection_times.1.y);

        Some(
            [
                (
                    self.calculate_position(time1) - other.calculate_position(time1),
                    time1,
                ),
                (
                    self.calculate_position(time2) - other.calculate_position(time2),
                    time2,
                ),
                (
                    self.calculate_position(time1_other) - other.calculate_position(time1_other),
                    time1_other,
                ),
                (
                    self.calculate_position(time2_other) - other.calculate_position(time2_other),
                    time2_other,
                ),
            ]
            .map(Intersection::from)
            .iter()
            .fold(Intersection::new(f64::INFINITY, f64::INFINITY), |a, b| {
                a.min_by_distance(b)
            }),
        )
    }

    fn collisions(
        &self,
        other: &Orbit,
        time: f64,
        collision_distance: f64,
    ) -> [Option<Collision>; 4] {
        if let Some(intersection_times) = self.intersection_times(other, time) {
            let (time1, time2) = (intersection_times.0.x, intersection_times.0.y);
            let (time1_other, time2_other) = (intersection_times.1.x, intersection_times.1.y);

            let intersection_distance1 =
                (self.calculate_position(time1) - other.calculate_position(time1)).length();
            let intersection_distance2 =
                (self.calculate_position(time2) - other.calculate_position(time2)).length();
            let intersection_distance1_other = (self.calculate_position(time1_other)
                - other.calculate_position(time1_other))
            .length();
            let intersection_distance2_other = (self.calculate_position(time2_other)
                - other.calculate_position(time2_other))
            .length();

            let collision1 = if intersection_distance1 < collision_distance
                && time1 - time < 0.75 * self.period()
            {
                Some(Collision::new(self.calculate_position(time1), time1 - time))
            } else {
                None
            };
            let collision2 = if intersection_distance1_other < collision_distance
                && time1_other - time < 0.75 * other.period()
            {
                Some(Collision::new(
                    other.calculate_position(time1_other),
                    time1_other - time,
                ))
            } else {
                None
            };
            let collision3 = if intersection_distance2 < collision_distance
                && time2 - time < 0.75 * self.period()
            {
                Some(Collision::new(self.calculate_position(time2), time2 - time))
            } else {
                None
            };
            let collision4 = if intersection_distance2_other < collision_distance
                && time2_other - time < 0.75 * other.period()
            {
                Some(Collision::new(
                    other.calculate_position(time2_other),
                    time2_other - time,
                ))
            } else {
                None
            };

            [collision1, collision2, collision3, collision4]
        } else {
            [None; 4]
        }
    }
}

#[derive(Copy, Clone)]
pub struct Intersection {
    pub distance: f64,
    pub time: f64,
}

impl Intersection {
    pub fn new(distance: f64, time: f64) -> Self {
        Self { distance, time }
    }

    pub fn min_by_distance(&self, other: &Intersection) -> Self {
        if self.distance <= other.distance {
            *self
        } else {
            *other
        }
    }
}

impl From<(DVec3, f64)> for Intersection {
    fn from(value: (DVec3, f64)) -> Self {
        Self::new(value.0.length(), value.1)
    }
}
