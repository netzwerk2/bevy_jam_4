use std::collections::HashMap;

use bevy::math::DVec3;
use bevy::prelude::*;
use bevy_mod_picking::prelude::Pickable;
use bevy_polyline::prelude::*;
use orbit_tessellation::{Orbit, OrbitBundle};

use crate::assets::{OrbitAssets, SpriteAssets};
use crate::game_state::{DespawnInMenu, GameState};
use crate::kessler_syndrome::Debris;
use crate::orbit_ext::{OrbitExt, TESSELATION_SETTINGS};
use crate::satellite::OrbitForSatellite;
use crate::time::{IngameTime, TimeMultiplier};
use crate::ui::COLLISION_Z_INDEX;

pub struct OrbitIntersectionPlugin;

impl Plugin for OrbitIntersectionPlugin {
    fn build(&self, app: &mut App) {
        app.init_resource::<SatelliteCollisionDistance>()
            .init_resource::<SatelliteCollisions>()
            .add_systems(
                PreUpdate,
                clear_collision_previews.run_if(in_state(GameState::InGame)),
            )
            .add_systems(
                Update,
                (
                    (
                        (update_satellite_collisions, clear_collision_previews),
                        create_collision_previews,
                    )
                        .chain(),
                    lose_game,
                )
                    .run_if(in_state(GameState::InGame)),
            );
    }
}

#[derive(Component)]
#[component(storage = "SparseSet")]
pub struct CollisionPreview;

#[derive(Deref, DerefMut, Resource)]
pub struct SatelliteCollisionDistance(pub f64);

impl Default for SatelliteCollisionDistance {
    fn default() -> Self {
        Self(80e3)
    }
}

#[derive(Copy, Clone)]
pub struct Collision {
    pub position: DVec3,
    pub delta_time: f64,
}

impl Collision {
    pub fn new(position: DVec3, delta_time: f64) -> Self {
        Self {
            position,
            delta_time,
        }
    }
}

#[derive(Default, Deref, DerefMut, Resource)]
pub struct SatelliteCollisions(pub HashMap<Entity, Collision>);

fn update_satellite_collisions(
    ingame_time: Res<IngameTime>,
    collision_distance: Res<SatelliteCollisionDistance>,
    mut satellite_collisions: ResMut<SatelliteCollisions>,
    orbit_query: Query<(Entity, &Orbit), With<Debris>>,
    satellite_query: Query<&Orbit, With<OrbitForSatellite>>,
) {
    satellite_collisions.clear();

    let satellite_orbit = satellite_query.single();

    for (entity, orbit) in orbit_query.iter() {
        for collision in satellite_orbit
            .collisions(orbit, **ingame_time, **collision_distance)
            .iter()
            .flatten()
        {
            satellite_collisions.insert(entity, *collision);
        }
    }
}

fn clear_collision_previews(
    mut commands: Commands,
    entities: Query<Entity, With<CollisionPreview>>,
) {
    for entity in entities.iter() {
        commands.entity(entity).despawn_recursive();
    }
}

fn create_collision_previews(
    mut commands: Commands,
    satellite_collisions: Res<SatelliteCollisions>,
    sprite_assets: Res<SpriteAssets>,
    mut polylines: ResMut<Assets<Polyline>>,
    orbit_assets: Res<OrbitAssets>,
    orbit_query: Query<&Orbit, With<Debris>>,
) {
    for (entity, collision) in satellite_collisions.iter() {
        commands
            .spawn(PbrBundle {
                mesh: sprite_assets.quad.clone_weak(),
                material: sprite_assets.collision_material.clone_weak(),
                transform: Transform::from_translation(Vec3::new(
                    collision.position.x as f32,
                    COLLISION_Z_INDEX,
                    collision.position.z as f32,
                ))
                .with_scale(Vec3::splat(1.2e6)),
                ..default()
            })
            .insert(CollisionPreview)
            .insert(Pickable::IGNORE)
            .insert(DespawnInMenu);
        commands
            .spawn(OrbitBundle {
                orbit: *orbit_query.get(*entity).unwrap(),
                tessellation_settings: TESSELATION_SETTINGS,
                polyline_bundle: PolylineBundle {
                    polyline: polylines.add(Polyline::default()),
                    material: orbit_assets.debris_orbit.clone_weak(),
                    ..default()
                },
            })
            .insert(CollisionPreview)
            .insert(DespawnInMenu);
    }
}

fn lose_game(
    mut next_state: ResMut<NextState<GameState>>,
    satellite_collisions: Res<SatelliteCollisions>,
    time: Res<Time>,
    time_multiplier: Res<TimeMultiplier>,
) {
    for collision in satellite_collisions.values() {
        if collision.delta_time < time.delta_seconds_f64() * **time_multiplier {
            next_state.set(GameState::LossScreen);
        }
    }
}
