use bevy::prelude::*;
use bevy_mod_picking::prelude::*;

use crate::game_state::GameState;
use crate::satellite::{Satellite, SelectedSatellite};

pub struct ManeuverPlugin;

impl Plugin for ManeuverPlugin {
    fn build(&self, app: &mut App) {
        app.init_resource::<ManeuverInput>().add_systems(
            Update,
            (maneuver_visibility, update_maneuver_input, cancel_maneuver)
                .run_if(in_state(GameState::InGame)),
        );
    }
}

#[derive(Component, Deref, DerefMut)]
pub struct DefaultPosition(pub Vec3);

#[derive(Component, Debug)]
pub enum ManeuverDirection {
    Prograde,
    Retrograde,
    RadialIn,
    RadialOut,
}

#[derive(Resource, Default, Debug)]
pub struct ManeuverInput {
    pub prograde: f64,
    pub radial_in: f64,
}

fn maneuver_visibility(
    selected_satellite: Res<SelectedSatellite>,
    mut maneuver_buttons: Query<(&mut Visibility, &ManeuverDirection, &Parent)>,
) {
    if let Some(selected_satellite) = **selected_satellite {
        for (mut visibility, _, parent) in &mut maneuver_buttons {
            if parent.get() == selected_satellite {
                *visibility = Visibility::Visible;
            } else {
                *visibility = Visibility::Hidden;
            }
        }
    } else {
        for (mut visibility, _, _) in &mut maneuver_buttons {
            *visibility = Visibility::Hidden;
        }
    }
}

pub fn maneuver_interaction(
    event: Listener<Pointer<Drag>>,
    mut buttons: Query<(
        &mut Transform,
        &ManeuverDirection,
        &DefaultPosition,
        &Parent,
    )>,
    satellites: Query<&GlobalTransform, With<Satellite>>,
    selected_satellite: Res<SelectedSatellite>,
    q_camera: Query<(&Camera, &GlobalTransform), With<Camera3d>>,
) {
    if selected_satellite.is_none() {
        return;
    }

    if let Ok((mut transform, direction, default_position, parent)) =
        buttons.get_mut(event.listener())
    {
        let (camera, camera_transform) = q_camera.single();
        let Some(delta) =
            camera.viewport_to_world(camera_transform, event.pointer_location.position)
        else {
            return;
        };

        let relative_pos = satellites
            .get(**parent)
            .unwrap()
            .affine()
            .inverse()
            .transform_point(delta.origin);

        let pos = match direction {
            ManeuverDirection::Prograde => relative_pos.z.min(default_position.z),
            ManeuverDirection::Retrograde => relative_pos.z.max(default_position.z),
            ManeuverDirection::RadialIn => relative_pos.x.min(default_position.x),
            ManeuverDirection::RadialOut => relative_pos.x.max(default_position.x),
        };

        match direction {
            ManeuverDirection::Prograde | ManeuverDirection::Retrograde => {
                transform.translation.z = pos;
            }
            ManeuverDirection::RadialIn | ManeuverDirection::RadialOut => {
                transform.translation.x = pos;
            }
        }
    }
}

fn update_maneuver_input(
    mut maneuver_input: ResMut<ManeuverInput>,
    buttons: Query<(&Transform, &ManeuverDirection, &DefaultPosition)>,
    time: Res<Time>,
) {
    for (transform, direction, default_position) in &buttons {
        let dt = time.delta_seconds_f64();
        let pos_delta = (transform.translation - **default_position).xz();
        let pos_delta = (pos_delta.x + pos_delta.y).abs();

        match direction {
            ManeuverDirection::Prograde => maneuver_input.prograde += dt * pos_delta as f64 * 1e-5,
            ManeuverDirection::Retrograde => {
                maneuver_input.prograde -= dt * pos_delta as f64 * 1e-5
            }
            ManeuverDirection::RadialIn => maneuver_input.radial_in += dt * pos_delta as f64 * 1e-5,
            ManeuverDirection::RadialOut => {
                maneuver_input.radial_in -= dt * pos_delta as f64 * 1e-5
            }
        }
    }
}

fn cancel_maneuver(
    input: Res<Input<KeyCode>>,
    selected_satellite: Res<SelectedSatellite>,
    mut maneuver_input: ResMut<ManeuverInput>,
) {
    if !input.just_pressed(KeyCode::Escape) {
        return;
    }

    if selected_satellite.is_some() {
        *maneuver_input = ManeuverInput::default();
    }
}
