use bevy::core_pipeline::clear_color::ClearColorConfig;
use bevy::prelude::shape::Plane;
use bevy::prelude::*;
use bevy_mod_picking::events::{Drag, DragEnd, DragStart, Pointer};
use bevy_mod_picking::picking_core::Pickable;
use bevy_mod_picking::prelude::{ListenerInput, NoDeselect, On};
use bevy_mod_picking::PickableBundle;
use bevy_screen_diagnostics::{
    Aggregate, ScreenDiagnostics, ScreenDiagnosticsPlugin, ScreenFrameDiagnosticsPlugin,
};

use crate::assets::{FontAssets, ManeuverAssets};
use crate::diagnostics::DiagnosticsPlugin;
use crate::game_state::{DespawnInMenu, GameState};
use crate::maneuver::{maneuver_interaction, DefaultPosition, ManeuverDirection, ManeuverInput};
use crate::satellite::Satellite;
use crate::time::IngameTime;

pub const DEBRIS_Z_INDEX: f32 = 5.0;
pub const SATELLITE_ORBIT_Z_INDEX: f32 = 8.0;
pub const EARTH_Z_INDEX: f32 = 10.0;
pub const SATELLITE_Z_INDEX: f32 = 20.0;
pub const COLLISION_Z_INDEX: f32 = 30.0;

pub struct UiPlugin;

#[derive(Event)]
pub struct DragEndEvent(pub Entity);

impl From<ListenerInput<Pointer<DragEnd>>> for DragEndEvent {
    fn from(value: ListenerInput<Pointer<DragEnd>>) -> Self {
        Self(value.listener())
    }
}
impl Plugin for UiPlugin {
    fn build(&self, app: &mut App) {
        app.add_plugins((
            ScreenDiagnosticsPlugin {
                style: Style {
                    align_self: AlignSelf::FlexEnd,
                    position_type: PositionType::Absolute,
                    top: Val::Px(15.0),
                    right: Val::Px(15.0),
                    ..default()
                },
                ..default()
            },
            ScreenFrameDiagnosticsPlugin,
        ))
        .add_systems(
            OnEnter(GameState::InGame),
            (setup_screen_diagnostics, setup_ui),
        )
        .add_systems(
            Update,
            spawn_maneuver_ui.run_if(in_state(GameState::InGame)),
        )
        .add_systems(
            Update,
            sync_delta_v_text.run_if(in_state(GameState::InGame)),
        )
        .add_event::<DragEndEvent>()
        .add_systems(
            PostUpdate,
            reset_arrow_positions.run_if(in_state(GameState::InGame)),
        );
    }
}

fn setup_screen_diagnostics(mut screen_diagnostics: ResMut<ScreenDiagnostics>) {
    screen_diagnostics
        .add("Debris".to_string(), DiagnosticsPlugin::DEBRIS_COUNT)
        .aggregate(Aggregate::Value)
        .format(|x| format!("{:.0}", x));
}

pub fn spawn_maneuver_ui(
    mut commands: Commands,
    satellites: Query<Entity, Added<Satellite>>,
    assets: Res<ManeuverAssets>,
    mut meshes: ResMut<Assets<Mesh>>,
    mut materials: ResMut<Assets<StandardMaterial>>,
) {
    let plane = meshes.add(Mesh::from(Plane::from_size(1.5e6)));
    let offset = 2.5e6;

    for satellite in &satellites {
        commands.entity(satellite).with_children(|parent| {
            parent.spawn((
                PbrBundle {
                    mesh: plane.clone(),
                    material: materials.add(StandardMaterial {
                        base_color_texture: Some(assets.prograde.clone_weak()),
                        unlit: true,
                        alpha_mode: AlphaMode::Mask(0.1),
                        ..default()
                    }),
                    transform: Transform::from_xyz(0.0, 0.0, -offset),
                    ..default()
                },
                DefaultPosition(Vec3::new(0.0, 0.0, -offset)),
                PickableBundle::default(),
                On::<Pointer<DragStart>>::target_insert(Pickable::IGNORE), // Disable picking
                On::<Pointer<DragEnd>>::send_event::<DragEndEvent>(),
                On::<Pointer<Drag>>::run(maneuver_interaction),
                NoDeselect,
                ManeuverDirection::Prograde,
            ));

            parent.spawn((
                PbrBundle {
                    mesh: plane.clone(),
                    material: materials.add(StandardMaterial {
                        base_color_texture: Some(assets.retrograde.clone_weak()),
                        unlit: true,
                        alpha_mode: AlphaMode::Mask(0.1),
                        ..default()
                    }),
                    transform: Transform::from_xyz(0.0, 0.0, offset),
                    ..default()
                },
                DefaultPosition(Vec3::new(0.0, 0.0, offset)),
                PickableBundle::default(),
                On::<Pointer<DragStart>>::target_insert(Pickable::IGNORE), // Disable picking
                On::<Pointer<Drag>>::run(maneuver_interaction),
                On::<Pointer<DragEnd>>::send_event::<DragEndEvent>(),
                NoDeselect,
                ManeuverDirection::Retrograde,
            ));

            parent.spawn((
                PbrBundle {
                    mesh: plane.clone(),
                    material: materials.add(StandardMaterial {
                        base_color_texture: Some(assets.radial_in.clone_weak()),
                        unlit: true,
                        alpha_mode: AlphaMode::Mask(0.1),
                        ..default()
                    }),
                    transform: Transform::from_xyz(-offset, 0.0, 0.0),
                    ..default()
                },
                DefaultPosition(Vec3::new(-offset, 0.0, 0.0)),
                PickableBundle::default(),
                On::<Pointer<DragStart>>::target_insert(Pickable::IGNORE), // Disable picking
                On::<Pointer<DragEnd>>::send_event::<DragEndEvent>(),
                On::<Pointer<Drag>>::run(maneuver_interaction),
                NoDeselect,
                ManeuverDirection::RadialIn,
            ));

            parent.spawn((
                PbrBundle {
                    mesh: plane.clone(),
                    material: materials.add(StandardMaterial {
                        base_color_texture: Some(assets.radial_out.clone_weak()),
                        unlit: true,
                        alpha_mode: AlphaMode::Mask(0.1),
                        ..default()
                    }),
                    transform: Transform::from_xyz(offset, 0.0, 0.0),
                    ..default()
                },
                DefaultPosition(Vec3::new(offset, 0.0, 0.0)),
                PickableBundle::default(),
                On::<Pointer<DragStart>>::target_insert(Pickable::IGNORE), // Disable picking
                On::<Pointer<DragEnd>>::send_event::<DragEndEvent>(),
                On::<Pointer<Drag>>::run(maneuver_interaction),
                NoDeselect,
                ManeuverDirection::RadialOut,
            ));
        });
    }
}

fn setup_ui(mut commands: Commands, font_assets: Res<FontAssets>) {
    commands
        .spawn(Camera2dBundle {
            camera_2d: Camera2d {
                clear_color: ClearColorConfig::None,
            },
            camera: Camera {
                order: 2,
                ..default()
            },
            ..default()
        })
        .insert(DespawnInMenu);

    commands
        .spawn(NodeBundle {
            style: Style {
                flex_direction: FlexDirection::Column,
                align_self: AlignSelf::FlexStart,
                position_type: PositionType::Absolute,
                top: Val::Px(15.0),
                left: Val::Px(15.0),
                ..default()
            },
            ..default()
        })
        .insert(DespawnInMenu)
        .with_children(|parent| {
            let style = TextStyle {
                font: font_assets.ubuntu_mono.clone_weak(),
                font_size: 20.0,
                ..default()
            };

            parent
                .spawn(TextBundle::from_sections([
                    TextSection::new("Remaining Δv: ", style.clone()),
                    TextSection::new("0.0 m/s", style.clone()),
                ]))
                .insert(DeltaVText);
            parent
                .spawn(TextBundle::from_sections([
                    TextSection::new("Maneuver Δv:  ", style.clone()),
                    TextSection::new("0.0 m/s", style.clone()),
                ]))
                .insert(ManeuverDeltaVText);
            parent
                .spawn(TextBundle::from_sections([
                    TextSection::new("Total Time Survived: ", style.clone()),
                    TextSection::new("0 h", style.clone()),
                ]))
                .insert(TimeSurvivedText);
        });
}

#[derive(Component)]
pub struct DeltaVText;

#[derive(Component)]
pub struct ManeuverDeltaVText;

#[derive(Component)]
pub struct TimeSurvivedText;

fn sync_delta_v_text(
    mut total_text: Query<&mut Text, With<DeltaVText>>,
    mut maneuver_text: Query<&mut Text, (With<ManeuverDeltaVText>, Without<DeltaVText>)>,
    mut time_text: Query<
        &mut Text,
        (
            With<TimeSurvivedText>,
            Without<ManeuverDeltaVText>,
            Without<DeltaVText>,
        ),
    >,
    delta_v: Query<&Satellite>,
    maneuver_input: Res<ManeuverInput>,
    ingame_time: Res<IngameTime>,
) {
    total_text.single_mut().sections[1].value = format!("{:.2} m/s", delta_v.single().delta_v);

    maneuver_text.single_mut().sections[1].value = format!(
        "{:>6.2} m/s",
        (maneuver_input.prograde.powi(2) + maneuver_input.radial_in.powi(2)).sqrt()
    );
    time_text.single_mut().sections[1].value = format!("{:.1} h", **ingame_time / 3600.0);
}

fn reset_arrow_positions(
    mut events: EventReader<DragEndEvent>,
    mut q: Query<(&mut Transform, &DefaultPosition, Entity)>,
    mut commands: Commands,
) {
    for DragEndEvent(entity) in events.read() {
        let Ok((mut transform, pos, entity)) = q.get_mut(*entity) else {
            continue;
        };
        commands.entity(entity).try_insert(Pickable::default());
        transform.translation = **pos;
    }
}
