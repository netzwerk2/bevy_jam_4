use std::cmp::Ordering;
use std::collections::{HashMap, HashSet};
use std::f32::consts::FRAC_PI_2;
use std::fmt::{Debug, Formatter};
use std::hash::{Hash, Hasher};
use std::sync::{Arc, Mutex};

use bevy::prelude::*;
use orbit_tessellation::Orbit;

use crate::kessler_syndrome::Debris;
use crate::time::{IngameTime, TimePlugin};
use crate::EARTH_RADIUS;

pub struct SpatialHashPlugin;

impl Plugin for SpatialHashPlugin {
    fn build(&self, app: &mut App) {
        app.init_resource::<SpatialHashGrid>()
            .add_plugins(TimePlugin)
            .add_systems(Update, (update_spatial_hash, insert_client_component));
    }
}

fn insert_client_component(
    mut commands: Commands,
    ingame_time: Res<IngameTime>,
    mut spatial_hash_grid: ResMut<SpatialHashGrid>,
    entities: Query<(Entity, &Orbit), (Added<Orbit>, With<Debris>)>,
) {
    for (entity, orbit) in &entities {
        let position = orbit.calculate_position(**ingame_time);
        let hash_position = Vec2::new(position.x as f32, -position.z as f32);

        commands
            .entity(entity)
            .insert(spatial_hash_grid.new_client(hash_position, entity));
    }
}

fn update_spatial_hash(
    ingame_time: Res<IngameTime>,
    mut orbits: Query<(&Orbit, &mut SpatialHashClient), With<Orbit>>,
    spatial_hash_grid: ResMut<SpatialHashGrid>,
) {
    let spatial_hash_grid = Arc::new(Mutex::new(spatial_hash_grid));

    orbits.par_iter_mut().for_each(|(orbit, mut client)| {
        let position = orbit.calculate_position(**ingame_time);
        let hash_position = Vec2::new(position.x as f32, -position.z as f32);

        let mut hash_grid = spatial_hash_grid.lock().unwrap();

        let key = hash_grid.get_cell_index(hash_position);

        if key != client.current_key {
            hash_grid.remove(client.as_ref());
            hash_grid.insert(hash_position, client.as_mut())
        }
    });
}

#[derive(Resource)]
pub struct SpatialHashGrid {
    pub bounds: (Vec2, Vec2),
    pub dimensions: UVec2,
    pub map: HashMap<UVec2, HashSet<SpatialHashClient>>,
}

impl Default for SpatialHashGrid {
    fn default() -> Self {
        SpatialHashGrid::new(
            (
                Vec2::new(-3.0 * EARTH_RADIUS, -3.0 * EARTH_RADIUS),
                Vec2::new(3.0 * EARTH_RADIUS, 3.0 * EARTH_RADIUS),
            ),
            UVec2::new(256, 256),
        )
    }
}

#[derive(Component, Copy, Clone)]
pub struct SpatialHashClient {
    pub entity: Entity,
    pub current_key: UVec2,
}

impl Debug for SpatialHashClient {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        self.entity.fmt(f)
    }
}

impl PartialEq<Self> for SpatialHashClient {
    fn eq(&self, other: &Self) -> bool {
        self.entity == other.entity
    }
}

impl Eq for SpatialHashClient {}

impl PartialOrd<Self> for SpatialHashClient {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}

impl Ord for SpatialHashClient {
    fn cmp(&self, other: &Self) -> Ordering {
        self.entity.cmp(&other.entity)
    }
}
impl Hash for SpatialHashClient {
    fn hash<H: Hasher>(&self, state: &mut H) {
        self.entity.hash(state)
    }
}

impl SpatialHashGrid {
    pub fn new(bounds: (Vec2, Vec2), dimensions: UVec2) -> Self {
        Self {
            bounds,
            dimensions,
            map: HashMap::new(),
        }
    }

    pub fn remove(&mut self, client: &SpatialHashClient) {
        self.map
            .get_mut(&client.current_key)
            .unwrap()
            .remove(client);
    }

    pub fn new_client(&mut self, position: Vec2, entity: Entity) -> SpatialHashClient {
        let mut client = SpatialHashClient {
            entity,
            current_key: UVec2::ZERO,
        };

        self.insert(position, &mut client);

        client
    }

    pub fn insert(&mut self, position: Vec2, client: &mut SpatialHashClient) {
        let key = self.get_cell_index(position);

        client.current_key = key;

        if let Some(cell) = self.map.get_mut(&key) {
            cell.insert(*client);
        } else {
            self.map.insert(key, HashSet::from([*client]));
        }
    }

    pub fn get_cell_index(&self, position: Vec2) -> UVec2 {
        let x = (position.x - self.bounds.0.x) / (self.bounds.1.x - self.bounds.0.x)
            * self.dimensions.x as f32;
        let y = (position.y - self.bounds.0.y) / (self.bounds.1.y - self.bounds.0.y)
            * self.dimensions.y as f32;

        UVec2::new(x as u32, y as u32).clamp(UVec2::new(0, 0), self.dimensions - UVec2::ONE)
    }

    pub fn find_near(&self, index: UVec2, range: UVec2) -> Vec<SpatialHashClient> {
        let i1 = index.saturating_sub(range);
        let i2 = index + range;

        let mut res: Vec<SpatialHashClient> = Vec::new();

        for x in i1.x..=i2.x {
            for y in i1.y..=i2.y {
                if let Some(clients) = self.map.get(&UVec2::new(x, y)) {
                    res.extend(clients);
                }
            }
        }

        res
    }
}

pub struct SpatialHashDebugPlugin;

impl Plugin for SpatialHashDebugPlugin {
    fn build(&self, app: &mut App) {
        app.add_systems(Update, draw_grid);
    }
}

fn draw_grid(hash_grid: Res<SpatialHashGrid>, mut gizmos: Gizmos) {
    let min_bounds = hash_grid.bounds.0;
    let max_bounds = hash_grid.bounds.1;
    let dimensions = hash_grid.dimensions;
    let grid_size = (max_bounds - min_bounds) / dimensions.as_vec2();

    let top_left = hash_grid.get_cell_index(min_bounds);
    let bottom_right = hash_grid.get_cell_index(max_bounds);

    for x in top_left.x..=bottom_right.x {
        for y in top_left.y..=bottom_right.y {
            let world_x = min_bounds.x + x as f32 * grid_size.x + grid_size.x * 0.5;
            let world_y = min_bounds.y + y as f32 * grid_size.y + grid_size.y * 0.5;

            if let Some(hash_set) = hash_grid.map.get(&UVec2::new(x, y)) {
                if !hash_set.is_empty() {
                    gizmos.rect(
                        Vec3::new(world_x, 20.0, -world_y),
                        Quat::from_rotation_x(FRAC_PI_2),
                        grid_size,
                        Color::RED,
                    );

                    continue;
                }
            }

            gizmos.rect(
                Vec3::new(world_x, 10.0, -world_y),
                Quat::from_rotation_x(FRAC_PI_2),
                grid_size,
                Color::YELLOW,
            );
        }
    }
}
