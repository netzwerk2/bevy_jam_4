use bevy::prelude::*;

pub struct TimePlugin;

impl Plugin for TimePlugin {
    fn build(&self, app: &mut App) {
        app.init_resource::<IngameTime>()
            .init_resource::<TimeMultiplier>()
            .add_systems(PreUpdate, update_time);
    }
}

#[derive(Default, Deref, DerefMut, Resource)]
pub struct IngameTime(pub f64);

#[derive(Deref, DerefMut, Resource)]
pub struct TimeMultiplier(pub f64);

impl Default for TimeMultiplier {
    fn default() -> Self {
        Self(500.0)
    }
}

fn update_time(
    time: Res<Time>,
    time_multiplier: Res<TimeMultiplier>,
    mut ingame_time: ResMut<IngameTime>,
) {
    **ingame_time += time.delta_seconds_f64() * **time_multiplier;
}
