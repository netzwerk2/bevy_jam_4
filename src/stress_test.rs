use std::f32::consts::FRAC_PI_2;
use std::f64::consts::{PI, TAU};

use bevy::prelude::*;
use bevy_turborand::{DelegatedRng, GlobalRng};
use orbit_tessellation::Orbit;

use crate::assets::DebrisAssets;
use crate::game_state::{DespawnInMenu, GameState};
use crate::kessler_syndrome::DebrisBundle;
use crate::{EARTH_MU, EARTH_RADIUS};

pub struct StressTestPlugin;

impl Plugin for StressTestPlugin {
    fn build(&self, app: &mut App) {
        app.add_systems(OnEnter(GameState::InGame), setup_orbits);
    }
}

fn setup_orbits(
    mut commands: Commands,
    mut rng: ResMut<GlobalRng>,
    debris_assets: Res<DebrisAssets>,
) {
    for _ in 0..1000 {
        let semi_major_axis = EARTH_RADIUS as f64 + 2000e3 + rng.f64() * 1000e3;
        let eccentricity = rng.f64() * 0.1;
        let inclination = if rng.bool() { 0.0 } else { PI };
        let argument_of_periapsis = rng.f64() * TAU;
        let mean_anomaly = rng.f64() * TAU;

        let orbit = Orbit::new(
            semi_major_axis,
            eccentricity,
            inclination,
            0.0,
            argument_of_periapsis,
            mean_anomaly,
            EARTH_MU,
            0.0,
        );

        commands
            .spawn(DebrisBundle {
                orbit,
                pbr_bundle: PbrBundle {
                    mesh: debris_assets.mesh.clone_weak(),
                    material: debris_assets.material.clone_weak(),
                    transform: Transform::from_rotation(Quat::from_rotation_x(-FRAC_PI_2)),
                    ..default()
                },
                ..default()
            })
            .insert(DespawnInMenu);
    }
}
