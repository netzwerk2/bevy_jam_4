use std::collections::HashMap;
use std::f32::consts::FRAC_PI_2;
use std::hash::{Hash, Hasher};
use std::sync::{Arc, Mutex};
use std::time::Duration;

use bevy::math::DVec3;
use bevy::prelude::*;
use bevy_turborand::{DelegatedRng, GlobalRng};
use orbit_tessellation::{Orbit, StateVectors};
use rayon::prelude::*;

use crate::assets::DebrisAssets;
use crate::game_state::{DespawnInMenu, GameState};
use crate::orbit_ext::{Intersection, OrbitExt};
use crate::spatial_hash::{SpatialHashClient, SpatialHashGrid, SpatialHashPlugin};
use crate::time::{IngameTime, TimeMultiplier};
use crate::ui::DEBRIS_Z_INDEX;
use crate::{EARTH_MU, EARTH_RADIUS};

const COLLISION_COOLDOWN: f32 = 1000.0;

pub struct KesslerSyndromePlugin;

impl Plugin for KesslerSyndromePlugin {
    fn build(&self, app: &mut App) {
        app.init_resource::<PotentialCollisions>()
            .init_resource::<MinCollisionDistance>()
            .add_plugins(SpatialHashPlugin)
            .add_systems(
                Update,
                (
                    (update_potential_collisions, collide_satellites).chain(),
                    update_debris_positions,
                )
                    .run_if(in_state(GameState::InGame)),
            )
            .add_systems(
                PostUpdate,
                despawn_debris.run_if(in_state(GameState::InGame)),
            );
    }
}

pub struct EntityPair(pub Entity, pub Entity);

impl From<(Entity, Entity)> for EntityPair {
    fn from(value: (Entity, Entity)) -> Self {
        Self(value.0, value.1)
    }
}

impl PartialEq for EntityPair {
    fn eq(&self, other: &Self) -> bool {
        self.0.min(self.1) == other.0.min(other.1) && self.0.max(self.1) == other.0.max(other.1)
    }
}

impl Eq for EntityPair {}

impl Hash for EntityPair {
    fn hash<H: Hasher>(&self, state: &mut H) {
        self.0.min(self.1).hash(state);
        self.0.max(self.1).hash(state);
    }
}

#[derive(Deref, DerefMut, Resource)]
pub struct MinCollisionDistance(pub f64);

impl Default for MinCollisionDistance {
    fn default() -> Self {
        Self(55e3)
    }
}

#[derive(Default, Deref, DerefMut, Resource)]
pub struct PotentialCollisions(pub HashMap<EntityPair, Intersection>);

impl PotentialCollisions {
    pub fn insert(&mut self, entities: (Entity, Entity), intersection: Intersection) {
        self.0.insert(EntityPair::from(entities), intersection);
    }

    pub fn contains(&self, entities: (Entity, Entity)) -> bool {
        self.contains_key(&EntityPair::from(entities))
    }
}

#[derive(Component, Deref, DerefMut)]
pub struct CooldownTimer(pub Timer);

impl Default for CooldownTimer {
    fn default() -> Self {
        Self(Timer::from_seconds(COLLISION_COOLDOWN, TimerMode::Once))
    }
}

#[derive(Component, Default)]
pub struct Debris;

#[derive(Default, Bundle)]
pub struct DebrisBundle {
    pub orbit: Orbit,
    pub pbr_bundle: PbrBundle,
    pub cooldown_timer: CooldownTimer,
    pub debris: Debris,
}

fn update_potential_collisions(
    hash_grid: Res<SpatialHashGrid>,
    ingame_time: Res<IngameTime>,
    mut potential_collisions: ResMut<PotentialCollisions>,
    time: Res<Time>,
    time_multiplier: Res<TimeMultiplier>,
    orbit_query: Query<&Orbit, With<Debris>>,
    timer_query: Query<&mut CooldownTimer, With<Debris>>,
) {
    potential_collisions.clear();

    let potential_collisions = Arc::new(Mutex::new(potential_collisions));
    let timer_query = Arc::new(Mutex::new(timer_query));

    hash_grid.map.par_iter().for_each(|(index, _)| {
        let clients = hash_grid.find_near(*index, UVec2::ONE);

        if clients.len() >= 2 {
            for client1 in clients.iter() {
                for client2 in clients.iter() {
                    let entities = (client1.entity, client2.entity);

                    let orbit1 = orbit_query.get(entities.0).unwrap();
                    let orbit2 = orbit_query.get(entities.1).unwrap();

                    let delta_time =
                        Duration::from_secs_f64(time.delta_seconds_f64() * **time_multiplier);

                    let mut timer_query = timer_query.lock().unwrap();
                    let mut timer1 = timer_query.get_mut(entities.0).unwrap();
                    timer1.tick(delta_time);
                    let mut timer2 = timer_query.get_mut(entities.1).unwrap();
                    timer2.tick(delta_time);

                    if client1 == client2
                        || potential_collisions.lock().unwrap().contains(entities)
                        || !timer_query.get(entities.0).unwrap().finished()
                        || !timer_query.get(entities.1).unwrap().finished()
                    {
                        continue;
                    }

                    if let Some(intersection) = orbit1.closest_intersection(orbit2, **ingame_time) {
                        potential_collisions
                            .lock()
                            .unwrap()
                            .insert(entities, intersection);
                        timer_query.get_mut(entities.0).unwrap().reset();
                        timer_query.get_mut(entities.1).unwrap().reset();
                    }
                }
            }
        }
    });
}

fn collide_satellites(
    mut commands: Commands,
    potential_collisions: Res<PotentialCollisions>,
    min_collision_distance: Res<MinCollisionDistance>,
    ingame_time: Res<IngameTime>,
    time_multiplier: Res<TimeMultiplier>,
    satellite_assets: Res<DebrisAssets>,
    mut orbit_query: Query<&mut Orbit, With<Debris>>,
    mut rng: ResMut<GlobalRng>,
) {
    for (entities, intersection) in potential_collisions.iter() {
        if intersection.distance < **min_collision_distance {
            if (intersection.time - **ingame_time) > **time_multiplier / 60.0 {
                continue;
            }

            let orbit1 = orbit_query.get(entities.0).unwrap();
            let orbit2 = orbit_query.get(entities.1).unwrap();

            let mut state_vectors_1 = orbit1.calculate_state_vectors(**ingame_time);
            let mut state_vectors_2 = orbit2.calculate_state_vectors(**ingame_time);

            let alpha_min: f64 = 0.1;
            let alpha_0 = 0.05;
            let rng_value = rng.f64_normalized() * alpha_0;
            let alpha: f64 = rng_value + alpha_min.copysign(rng_value);

            let velocity_diff = state_vectors_1.velocity - state_vectors_2.velocity;
            let u = DVec3::new(velocity_diff.z * alpha, 0.0, -velocity_diff.x * alpha);

            let new_velocity = if rng.bool() {
                state_vectors_1.velocity + u
            } else {
                state_vectors_2.velocity + u
            };

            let u_half = u * 0.5;

            let new_orbit = Orbit::from_state_vectors(
                StateVectors::new(state_vectors_1.position, new_velocity),
                orbit1.gravitational_parameter,
                **ingame_time,
            );

            state_vectors_1.velocity -= u_half;
            state_vectors_2.velocity -= u_half;

            *orbit_query.get_mut(entities.0).unwrap() =
                Orbit::from_state_vectors(state_vectors_1, EARTH_MU, **ingame_time);
            *orbit_query.get_mut(entities.1).unwrap() =
                Orbit::from_state_vectors(state_vectors_2, EARTH_MU, **ingame_time);

            commands
                .spawn(DebrisBundle {
                    orbit: new_orbit,
                    pbr_bundle: PbrBundle {
                        mesh: satellite_assets.mesh.clone_weak(),
                        material: satellite_assets.collision_material.clone_weak(),
                        transform: Transform::from_rotation(Quat::from_rotation_x(-FRAC_PI_2))
                            .with_scale(Vec3::splat(1.0)),
                        ..default()
                    },
                    ..default()
                })
                .insert(DespawnInMenu);
        }
    }
}

fn update_debris_positions(
    ingame_time: Res<IngameTime>,
    mut orbit_query: Query<(&Orbit, &mut Transform), With<Debris>>,
) {
    for (orbit, mut transform) in orbit_query.iter_mut() {
        let position = orbit.calculate_position(**ingame_time);

        transform.translation = position.as_vec3() + Vec3::new(0.0, DEBRIS_Z_INDEX, 0.0);
    }
}

fn despawn_debris(
    mut debris_query: Query<(&Transform, Entity, &SpatialHashClient), With<Debris>>,
    mut hash_grid: ResMut<SpatialHashGrid>,
    mut commands: Commands,
) {
    for (transform, entity, client) in debris_query.iter_mut() {
        if transform.translation.length() < EARTH_RADIUS {
            commands.entity(entity).despawn();
            hash_grid.remove(client);
        }
    }
}
