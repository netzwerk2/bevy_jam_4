use bevy::prelude::*;

use crate::assets::FontAssets;
use crate::kessler_syndrome::{MinCollisionDistance, PotentialCollisions};
use crate::maneuver::ManeuverInput;
use crate::orbit_intersection::{SatelliteCollisionDistance, SatelliteCollisions};
use crate::reinit_resources;
use crate::satellite::SelectedSatellite;
use crate::spatial_hash::SpatialHashGrid;
use crate::time::{IngameTime, TimeMultiplier};

const NORMAL_BUTTON: Color = Color::rgb(0.15, 0.15, 0.15);
const HOVERED_BUTTON: Color = Color::rgb(0.25, 0.25, 0.25);
const PRESSED_BUTTON: Color = Color::rgb(0.35, 0.75, 0.35);

#[derive(Debug, Clone, Eq, PartialEq, Hash, Default, States)]
pub enum GameState {
    #[default]
    InGame,
    LossScreen,
}

pub struct GameStatePlugin;

impl Plugin for GameStatePlugin {
    fn build(&self, app: &mut App) {
        app.add_state::<GameState>()
            .add_systems(
                OnEnter(GameState::LossScreen),
                (despawn_all, setup_loss_screen).chain(),
            )
            .add_systems(
                OnExit(GameState::LossScreen),
                (reset_resources, despawn_all),
            )
            .add_systems(
                Update,
                button_system.run_if(in_state(GameState::LossScreen)),
            );
    }
}

#[derive(Component)]
pub struct DespawnInMenu;

fn despawn_all(mut commands: Commands, entities: Query<Entity, With<DespawnInMenu>>) {
    for entity in &entities {
        commands.entity(entity).despawn_recursive();
    }
}
fn reset_resources(mut commands: Commands) {
    reinit_resources!(
        commands,
        PotentialCollisions,
        SatelliteCollisionDistance,
        SelectedSatellite,
        IngameTime,
        TimeMultiplier,
        ManeuverInput,
        SatelliteCollisions,
        MinCollisionDistance,
        SpatialHashGrid
    );
}

fn setup_loss_screen(mut commands: Commands, ingame_time: Res<IngameTime>, fonts: Res<FontAssets>) {
    commands
        .spawn(Camera2dBundle {
            camera_2d: Camera2d { ..default() },
            camera: Camera {
                order: 2,
                ..default()
            },
            ..default()
        })
        .insert(DespawnInMenu);
    commands
        .spawn(NodeBundle {
            style: Style {
                // width: Val::Px(250.0)
                width: Val::Percent(100.0),
                height: Val::Percent(100.0),
                align_items: AlignItems::Center,
                justify_content: JustifyContent::Center,
                flex_direction: FlexDirection::Column,
                border: UiRect::default(),
                ..default()
            },
            ..default()
        })
        .insert(DespawnInMenu)
        .with_children(|parent| {
            parent.spawn(TextBundle::from_section(
                format!("You survived {:.1} hours.", **ingame_time / 3600.0),
                TextStyle {
                    font_size: 20.0,
                    ..default()
                },
            ));

            parent
                .spawn(ButtonBundle {
                    style: Style {
                        width: Val::Px(150.0),
                        height: Val::Px(65.0),
                        border: UiRect::all(Val::Px(5.0)),
                        // horizontally center child text
                        justify_content: JustifyContent::Center,
                        // vertically center child text
                        align_items: AlignItems::Center,
                        ..default()
                    },
                    border_color: BorderColor(Color::BLACK),
                    background_color: Color::WHITE.into(),
                    ..default()
                })
                .with_children(|parent| {
                    parent.spawn(TextBundle::from_section(
                        "Restart",
                        TextStyle {
                            font: fonts.ubuntu_mono.clone(),
                            font_size: 40.0,
                            color: Color::rgb(0.9, 0.9, 0.9),
                        },
                    ));
                });
        });
}
fn button_system(
    mut interaction_query: Query<
        (&Interaction, &mut BackgroundColor, &mut BorderColor),
        (Changed<Interaction>, With<Button>),
    >,
    mut next_state: ResMut<NextState<GameState>>,
) {
    for (interaction, mut color, mut border_color) in &mut interaction_query {
        match *interaction {
            Interaction::Pressed => {
                *color = PRESSED_BUTTON.into();
                border_color.0 = Color::RED;
                next_state.set(GameState::InGame)
            }
            Interaction::Hovered => {
                *color = HOVERED_BUTTON.into();
                border_color.0 = Color::WHITE;
            }
            Interaction::None => {
                *color = NORMAL_BUTTON.into();
                border_color.0 = Color::BLACK;
            }
        }
    }
}
