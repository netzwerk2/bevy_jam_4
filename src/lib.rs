#![allow(clippy::type_complexity)]
#![allow(clippy::too_many_arguments)]

use std::f32::consts::FRAC_PI_2;

use bevy::asset::AssetMetaCheck;
use bevy::core_pipeline::tonemapping::Tonemapping;
use bevy::diagnostic::{FrameTimeDiagnosticsPlugin, LogDiagnosticsPlugin};
use bevy::prelude::*;
use bevy::render::camera::{NormalizedRenderTarget, ScalingMode};
use bevy::window::{PresentMode, PrimaryWindow, WindowRef};
use bevy_mod_picking::pointer::Location;
use bevy_mod_picking::prelude::*;
use bevy_polyline::prelude::*;
use bevy_turborand::prelude::RngPlugin;
use orbit_tessellation::{Orbit, OrbitBundle, OrbitTessellationPlugin, TessellationCamera};

use crate::assets::AssetsPlugin;
use crate::assets::SpriteAssets;
use crate::diagnostics::DiagnosticsPlugin;
use crate::game_state::{DespawnInMenu, GameState, GameStatePlugin};
use crate::kessler_syndrome::KesslerSyndromePlugin;
use crate::maneuver::ManeuverPlugin;
use crate::orbit_ext::TESSELATION_SETTINGS;
use crate::orbit_intersection::OrbitIntersectionPlugin;
use crate::satellite::{
    remove_selected_satellite, set_selected_satellite, OrbitForSatellite, SatelliteBundle,
    SatellitePlugin,
};
use crate::stress_test::StressTestPlugin;
use crate::ui::{UiPlugin, EARTH_Z_INDEX, SATELLITE_ORBIT_Z_INDEX, SATELLITE_Z_INDEX};

mod assets;

mod diagnostics;
mod game_state;
mod kessler_syndrome;
mod maneuver;
mod orbit_ext;
mod orbit_intersection;
mod satellite;
mod spatial_hash;
mod stress_test;
mod time;
mod ui;

pub const EARTH_RADIUS: f32 = 6378e3;
pub const EARTH_MU: f64 = 3.9860044188e14;

#[macro_export]
macro_rules! reinit_resources {
    ($commands:ident, $t:ty) => {
        $commands.remove_resource::<$t>();
        $commands.init_resource::<$t>();
    };
    ($commands:ident, $t:ty, $($rest:ty), +) => {
        $commands.remove_resource::<$t>();
        $commands.init_resource::<$t>();
        reinit_resources!($commands, $($rest),+);
    };
}

pub struct GamePlugin;

impl Plugin for GamePlugin {
    fn build(&self, app: &mut App) {
        app.insert_resource(AssetMetaCheck::Never)
            .add_plugins((
                DefaultPlugins.set(WindowPlugin {
                    primary_window: Some(Window {
                        title: "High in Space".into(),
                        present_mode: PresentMode::AutoNoVsync,
                        fit_canvas_to_parent: true,
                        ..default()
                    }),
                    ..default()
                }),
                LogDiagnosticsPlugin::default(),
                FrameTimeDiagnosticsPlugin,
                DefaultPickingPlugins
                    .build()
                    .disable::<DebugPickingPlugin>()
                    .disable::<DefaultHighlightingPlugin>(),
            ))
            .add_plugins((
                GameStatePlugin,
                OrbitTessellationPlugin,
                OrbitIntersectionPlugin,
                RngPlugin::default(),
                KesslerSyndromePlugin,
                StressTestPlugin,
                SatellitePlugin,
                ManeuverPlugin,
                DiagnosticsPlugin,
            ))
            .add_plugins((AssetsPlugin, UiPlugin))
            .insert_resource(ClearColor(Color::BLACK))
            .add_systems(OnEnter(GameState::InGame), (setup, setup_satellite));
    }
}

fn setup(
    mut commands: Commands,
    mut meshes: ResMut<Assets<Mesh>>,
    mut materials: ResMut<Assets<StandardMaterial>>,
) {
    commands
        .spawn(Camera3dBundle {
            projection: Projection::Orthographic(OrthographicProjection {
                near: -1000.0,
                scaling_mode: ScalingMode::AutoMin {
                    min_width: EARTH_RADIUS * 4.0,
                    min_height: EARTH_RADIUS * 4.0,
                },
                ..default()
            }),
            tonemapping: Tonemapping::None,
            transform: Transform::from_xyz(0.0, SATELLITE_Z_INDEX + 10.0, 0.0)
                .looking_at(Vec3::ZERO, Vec3::NEG_Z),
            ..default()
        })
        .insert(TessellationCamera)
        .insert(DespawnInMenu);

    commands
        .spawn(PbrBundle {
            mesh: meshes.add(
                shape::Circle {
                    radius: EARTH_RADIUS,
                    ..default()
                }
                .into(),
            ),
            material: materials.add(StandardMaterial {
                base_color: Color::rgb(0.043, 0.604, 0.929),
                unlit: true,
                ..default()
            }),
            transform: Transform::from_rotation(Quat::from_rotation_x(-FRAC_PI_2))
                .with_translation(Vec3::new(0.0, EARTH_Z_INDEX, 0.0)),
            ..default()
        })
        .insert(DespawnInMenu);
}

fn setup_satellite(
    mut commands: Commands,
    assets: Res<SpriteAssets>,
    mut meshes: ResMut<Assets<Mesh>>,
    mut polylines: ResMut<Assets<Polyline>>,
    mut polyline_materials: ResMut<Assets<PolylineMaterial>>,
    mut selections: EventWriter<Pointer<Select>>,
    window_query: Query<Entity, With<PrimaryWindow>>,
) {
    let semi_major_axis = EARTH_RADIUS as f64 + 2500e3;
    let eccentricity = 0.0;
    let inclination = 0.0;
    let argument_of_periapsis = 0.0;
    let mean_anomaly = 0.0;

    let orbit = Orbit::new(
        semi_major_axis,
        eccentricity,
        inclination,
        0.0,
        argument_of_periapsis,
        mean_anomaly,
        EARTH_MU,
        0.0,
    );

    let id = commands
        .spawn(SatelliteBundle::new(orbit, &assets, &mut meshes))
        .insert(On::<Pointer<Select>>::run(set_selected_satellite))
        .insert(On::<Pointer<Deselect>>::run(remove_selected_satellite))
        .insert(DespawnInMenu)
        .id();

    selections.send(Pointer::new(
        PointerId::Mouse,
        Location {
            position: orbit.calculate_position(0.0).xz().as_vec2(),
            target: NormalizedRenderTarget::Window(
                WindowRef::Primary
                    .normalize(Some(window_query.single()))
                    .unwrap(),
            ),
        },
        id,
        Select,
    ));

    commands
        .spawn(OrbitBundle {
            orbit,
            tessellation_settings: TESSELATION_SETTINGS,
            polyline_bundle: PolylineBundle {
                polyline: polylines.add(Polyline::default()),
                material: polyline_materials.add(PolylineMaterial {
                    width: 1.0,
                    ..default()
                }),
                transform: Transform::from_xyz(0.0, SATELLITE_ORBIT_Z_INDEX, 0.0),
                ..default()
            },
        })
        .insert(OrbitForSatellite(id))
        .insert(DespawnInMenu);
}
