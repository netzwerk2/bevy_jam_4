use bevy::prelude::shape::Plane;
use bevy::prelude::*;
use bevy_polyline::prelude::*;

pub struct AssetsPlugin;

impl Plugin for AssetsPlugin {
    fn build(&self, app: &mut App) {
        app.init_resource::<SpriteAssets>()
            .init_resource::<ManeuverAssets>()
            .init_resource::<DebrisAssets>()
            .init_resource::<OrbitAssets>()
            .init_resource::<FontAssets>();
    }
}

#[derive(Resource)]
pub struct DebrisAssets {
    pub mesh: Handle<Mesh>,
    pub material: Handle<StandardMaterial>,
    pub collision_material: Handle<StandardMaterial>,
}

impl FromWorld for DebrisAssets {
    fn from_world(world: &mut World) -> Self {
        let mut meshes = world.resource_mut::<Assets<Mesh>>();
        let mesh = meshes.add(shape::Circle::new(30e3).into());

        let mut materials = world.resource_mut::<Assets<StandardMaterial>>();
        let material = materials.add(StandardMaterial {
            base_color: Color::WHITE,
            unlit: true,
            ..default()
        });
        let collision_material = materials.add(StandardMaterial {
            base_color: Color::GREEN,
            unlit: true,
            ..default()
        });

        Self {
            mesh,
            material,
            collision_material,
        }
    }
}

#[derive(Resource)]
pub struct SpriteAssets {
    pub quad: Handle<Mesh>,
    pub satellite_material: Handle<StandardMaterial>,
    pub collision_material: Handle<StandardMaterial>,
}

impl FromWorld for SpriteAssets {
    fn from_world(world: &mut World) -> Self {
        let mut meshes = world.resource_mut::<Assets<Mesh>>();
        let mesh = meshes.add(Mesh::from(Plane::default()));

        let asset_server = world.resource::<AssetServer>();

        let satellite_texture = asset_server.load("sprites/satellite.png");
        let collision_texture = asset_server.load("sprites/collision.png");

        let mut materials = world.resource_mut::<Assets<StandardMaterial>>();

        let satellite_material = materials.add(StandardMaterial {
            base_color_texture: Some(satellite_texture),
            unlit: true,
            alpha_mode: AlphaMode::Blend,
            ..default()
        });
        let collision_material = materials.add(StandardMaterial {
            base_color_texture: Some(collision_texture),
            unlit: true,
            alpha_mode: AlphaMode::Blend,
            ..default()
        });

        SpriteAssets {
            quad: mesh,
            satellite_material,
            collision_material,
        }
    }
}

#[derive(Resource)]
pub struct ManeuverAssets {
    pub prograde: Handle<Image>,
    pub retrograde: Handle<Image>,
    pub radial_in: Handle<Image>,
    pub radial_out: Handle<Image>,
}

impl FromWorld for ManeuverAssets {
    fn from_world(world: &mut World) -> Self {
        let asset_server = world.resource::<AssetServer>();

        Self {
            prograde: asset_server.load("sprites/prograde.png"),
            retrograde: asset_server.load("sprites/retrograde.png"),
            radial_in: asset_server.load("sprites/radial_in.png"),
            radial_out: asset_server.load("sprites/radial_out.png"),
        }
    }
}

#[derive(Resource)]
pub struct OrbitAssets {
    pub valid_orbit: Handle<PolylineMaterial>,
    pub invalid_orbit: Handle<PolylineMaterial>,
    pub debris_orbit: Handle<PolylineMaterial>,
}

impl FromWorld for OrbitAssets {
    fn from_world(world: &mut World) -> Self {
        let mut polyline_materials = world.resource_mut::<Assets<PolylineMaterial>>();

        let valid_orbit = polyline_materials.add(PolylineMaterial {
            color: Color::rgb(0.898, 0.969, 0.988),
            width: 2.0,
            ..default()
        });
        let invalid_orbit = polyline_materials.add(PolylineMaterial {
            color: Color::RED,
            width: 2.0,
            ..default()
        });
        let debris_orbit = polyline_materials.add(PolylineMaterial {
            color: Color::rgb(0.808, 0.588, 0.177),
            width: 1.0,
            ..default()
        });

        Self {
            valid_orbit,
            invalid_orbit,
            debris_orbit,
        }
    }
}

#[derive(Resource)]
pub struct FontAssets {
    pub ubuntu_mono: Handle<Font>,
}

impl FromWorld for FontAssets {
    fn from_world(world: &mut World) -> Self {
        let asset_server = world.resource::<AssetServer>();

        let ubuntu_light = asset_server.load("fonts/UbuntuMono-R.ttf");

        Self {
            ubuntu_mono: ubuntu_light,
        }
    }
}
