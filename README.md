## Before you play
This game relies heavily on the simulation of orbital trajectories and calculating their intersections.
Therefore, it is recommended to play the native build of this game in order to leverage the full multithreading capabilities of your system.

The source code can be found [here](https://gitlab.com/netzwerk2/bevy_jam_4).


## Aim of the Game

You control a satellite and need to make sure that it does not collide with any space debris.
However, as time progresses, the space debris will be become increasingly more,
due to collision with other space debris (known as the [Kessler syndrome](https://en.wikipedia.org/wiki/Kessler_syndrome)).
This newly created debris is highlighted in green.
Your goal is to survive as long as possible, which is also made difficult by limited fuel reserves.

## How to play

### Maneuvers

The game starts paused and gives you the ability to change your trajectory.
You can modify your trajectory by thrusting in one of four different directions:
Prograde, retrograde, radial and anti-radial.
You don't need to know how they change your trajectory off the top of your head,
simply experiment with them by dragging the arrows.
Dragging an arrow to a specific position adds a velocity change to the maneuver over time.
The further you drag it, the faster this happens.
So even if you need to make a large maneuver, you can later adjust it in small steps.
If you're interested in details, you can read more about these maneuver nodes
[here](https://wiki.kerbalspaceprogram.com/wiki/Maneuver_node).

To reset a maneuver, simply press ESC.
Once you're happy with your trajectory, simply click somewhere on the screen.
Now the game is unpaused and everything starts moving.
If you see a new collision coming up, just click on the satellite and the time is paused again,
allowing you to plan your maneuver.

### Fuel and Δv

In rocket science, the central resource of a vehicle is Δv, the change of velocity.
While it is essentially the amount of fuel that determines this (see
the [rocket equation](https://en.wikipedia.org/wiki/Tsiolkovsky_rocket_equation) for more information),
you never really talk about it. All you need is Δv.
Every maneuver requires a certain Δv, which you can see in the top left corner.
Once you have no Δv left, you can't make any further maneuvers, because you can't change the satellite's velocity anymore.
If your maneuver exceeded your Δv budget, the satellite's trajectory is drawn red.

### Credits

- [bevy](https://github.com/bevyengine/bevy)
- [bevy_mod_picking](https://github.com/aevyrie/bevy_mod_picking/)
- [bevy_screen_diagnostics](https://github.com/laundmo/bevy_screen_diagnostics)
- [rayon](https://github.com/rayon-rs/rayon)
- [bevy_polyline](https://github.com/rszemplinski/bevy_polyline)
- [orbit_tessellation](https://gitlab.com/highinspace/orbit_tessellation)
- [Ubuntu font](https://design.ubuntu.com/font)